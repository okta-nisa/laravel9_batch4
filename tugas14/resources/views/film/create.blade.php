@extends('layout.master')

@section('judul')
Create Film
@endsection

@section('content')
    <h1>Create Film</h1>

    <form action="{{ route('storeFilm') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="judul">Judul :</label>
            <input class="form-control" type="text" name= "judul" id='judul' class="form-control">
        </div>
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <div class="form-group">
            <label for="ringkasan">Ringkasan :</label>
            <textarea class="form-control" type="text" name= "ringkasan" id='ringkasan'></textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <div class="form-group">
            <label for="tahun">Tahun :</label>
            <input class="form-control" type="text" name= "tahun" id='tahun'>
        </div>
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        
        <div class="form-group">
            <label for="poster">Poster :</label>
            <input class="form-control" type="text" name= "poster" id='poster'>
        </div>
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        
        {{-- <input type="text" name= "genre" id='genre'><br><br> --}}
        <div class="form-group">
            <label for="genre">Genre :</label>
            <select class="form-control" name="genre">
                @foreach ($genres as $genre)
                    <option value="{{ $genre->id }}">{{ $genre->nama }}</option>
                @endforeach
                
            </select>

        </div>
        @error('genre')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
   
        <button type="submit">Create</button>

    </form>
@endsection