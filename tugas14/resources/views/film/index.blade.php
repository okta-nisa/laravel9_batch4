@extends('layout.master')

@section('judul')
Data Film
@endsection

@push('scripts')
    <script>    
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>Id</th>
      <th>Judul</th>
      <th>Ringkasan</th>
      <th>Tahun</th>
      <th>Poster</th>
      <th>Genre</th>

      <th>List Peran</th>
    </tr>
    </thead>
    <tbody>
      @forelse ($film as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->judul}}</td>
            <td>{{$value->ringkasan}}</td>
            <td>{{$value->tahun}}</td>
            <td>{{$value->poster}}</td>
            <td>{{$value->genre->nama}}</td>
           
            <td>
                
                @auth
                    {{-- <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a> --}}
                    <form style="display: inline" action="/peran/{{$value->id}}" method="POST">
                        @csrf
                        <input name="nama" id="nama" class="form-control">
                        @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                            
                        @enderror
                        <input type="submit" class="btn btn-info my-1" value="Tambah Peran">
                    </form>
                @endauth
                <a href="/film/{{$value->id}}" class="btn btn-warning">List Peran</a>
                
            </td>
        </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse  
    </tbody>
  </table>
@endsection
