@extends('layout.master')

@section('judul')
Detail Cast
@endsection

@section('content')
    <h1>Detail Cast</h1>

    <form action="{{ route('storeCast') }}" method="POST">
        @csrf
        <label for="first_name">Nama :</label><br>
        <input type="text" name= "nama" value="{{$cast->nama}}" disabled><br><br>

        <label for="last_name">Umur :</label><br>
        <input type="text" name="umur" value="{{$cast->umur}}" disabled><br><br>

        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10" disabled>{{$cast->bio}}</textarea><br>

    </form>
@endsection