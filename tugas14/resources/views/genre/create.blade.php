@extends('layout.master')

@section('judul')
Create Genre
@endsection

@section('content')
    <h1>Create Genre</h1>

    <form action="{{ route('storeGenre') }}" method="POST">
        @csrf
        <label for="nama">Nama :</label><br>
        <input type="text" name= "nama" id='nama'><br><br>
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
   
        <button type="submit">Create</button>

    </form>
@endsection