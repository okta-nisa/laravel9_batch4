@extends('layout.master')

@section('judul')
Data Genre
@endsection

@push('scripts')
    <script>    
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>Id</th>
      <th>Nama</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->nama}}</td>
           
            <td>
                <a href="/genre/{{$value->id}}" class="btn btn-info">Show</a>
                {{-- @auth
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form style="display: inline" action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                @endauth --}}
                
            </td>
        </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse  
    </tbody>
  </table>
@endsection
