@extends('layout.master')

@section('judul')
Create Cast
@endsection

@section('content')
    <h1>Create Cast</h1>
    
    <h3>Sign Up Form</h3>

    <form action="{{ route('storeCast') }}" method="POST">
        @csrf
        <label for="first_name">Nama :</label><br>
        <input type="text" name= "nama" id='nama'><br><br>
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        
        

        <label for="last_name">Umur :</label><br>
        <input type="text" name="umur" id="umur"><br><br>
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        


        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <button type="submit">Create</button>

    </form>
@endsection