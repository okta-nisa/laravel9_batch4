@extends('layout.master')

@section('judul')
Edit Cast
@endsection

@section('content')
    <h1>Edit Cast</h1>

    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <label for="first_name">Nama :</label><br>
        <input type="text" name= "nama" value="{{$cast->nama}}" id='nama'><br><br>
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <label for="last_name">Umur :</label><br>
        <input type="text" name="umur" value="{{$cast->umur}}" id='umur'><br><br>
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10" id='bio'>{{$cast->bio}}</textarea><br><br>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror


        <button type="submit">Create</button>

    </form>
@endsection