{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}


@extends('layout.master')

@section('judul')
Halaman Home
@endsection

@section('content')
<h1>Media Online</h1>

    <h2>Social Media Developer</h2>

    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

    <h3>Benefit Join di Media Online</h3>
    <ul>
        <li>Medapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    
    <h3>Cara Bergabung ke Media online</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="{{ route('register') }}">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection