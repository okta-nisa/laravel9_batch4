@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    
    <h3>Sign Up Form</h3>

    <form action="{{ route('halamanWelcome') }}" method="POST">
        @csrf
        <label for="first_name">First Name :</label><br>
        <input type="text" name= "first_name"><br><br>

        <label for="last_name">Last Name</label><br>
        <input type="text" name="last_name"><br><br>

        <label for="gender">Gender</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br><br>

        <label for="natinality">Nationality</label><br>
        <select name="nationality" id="nationality">
            <option value="">Indonesia</option>
            <option value="">Amerika</option>
            <option value="">Inggris</option>
        </select><br><br>

        <label for="language">Language Spoken</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>

        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <button type="submit">Sign Up</button>

    </form>
@endsection