@extends('layout.master')

@section('judul')
Halaman Update Profile
@endsection

@section('content')
    <form action="profile/{{ $detailProfile->id }}" method="POST">
        @csrf
        @method('put')

        <div class='form-group'>
            <label for="name">Nama :</label>
            <input type="text" value = "{{ $detailProfile->user->name }}" class="form-control" disabled>
        </div>
        <div class='form-group'>
            <label for="email">E-mail :</label>
            <input type="text" value = "{{ $detailProfile->user->email }}" class="form-control" disabled>
        </div>

        <div class='form-group'>
            <label for="umur">Umur :</label>
            <input type="text" name="umur" id="umur"  value = "{{ $detailProfile->umur }}" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <div class='form-group'>
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{ $detailProfile->bio }}</textarea><br>
        </div>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <div class='form-group'>
            <label for="alamat">Alamat</label><br>
            <textarea name="alamat" id="alamat" cols="30" rows="10"  class="form-control">{{ $detailProfile->alamat }}</textarea><br>
        </div>
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

        <button type="submit">Create</button>

    </form>
@endsection
