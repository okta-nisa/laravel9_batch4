<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('template/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      @auth
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }} ({{ Auth::user()->profile->umur }})</a>
        </div>
      @endauth
      @guest
      <div class="info">
        <a href="#" class="d-block">Belum Login</a>
      </div>
      @endguest
      
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{ route('halamanUtama') }}" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
              {{-- <i class="right fas fa-angle-left"></i> --}}
            </p>
          </a>
          
        </li>
        <li class="nav-item">
          <a href="../widgets.html" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Table
              {{-- <span class="right badge badge-danger">New</span> --}}
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('halamanDataTable') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Table</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="../widgets.html" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Genre
              {{-- <span class="right badge badge-danger">New</span> --}}
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('indexGenre') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Genre</p>
              </a>
            </li>
          </ul>
          @auth
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('createGenre') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create Genre</p>
                </a>
              </li>
            </ul>
          @endauth
        </li>
        <li class="nav-item">
          <a href="../widgets.html" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Film
              {{-- <span class="right badge badge-danger">New</span> --}}
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('indexFilm') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Film</p>
              </a>
            </li>
          </ul>
          @auth
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('createFilm') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create Film</p>
                </a>
              </li>
            </ul>
          @endauth
        </li>
        <li class="nav-item">
          <a href="../widgets.html" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Cast
              {{-- <span class="right badge badge-danger">New</span> --}}
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('indexCast') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Cast</p>
              </a>
            </li>
          </ul>
          @auth
          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="far fa-user nav-icon"></i>
              <p>Profile</p>
            </a>
          </li>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('createCast') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create Cast</p>
                </a>
              </li>
            </ul>
          @endauth
        </li>
        
        @guest
        
          <li class="nav-item bg-info">
            <a href="/login" class="nav-link">
              <i class="nav-icon fa fa-sign-out"></i>
              <p>
                Login
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </a>
          </li>
        @endguest
        @auth
        <li class="nav-item bg-danger">
          <a class="nav-link" href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>
        </li>
        @endauth
        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>