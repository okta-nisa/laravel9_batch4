<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = 'films'; 

    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster'];

    public function genre(){

        return $this->belongsTo(Genre::class, 'genre_id');
    }

    public function peran(){

        return $this->hasMany(Peran::class, 'film_id');
    }
}
