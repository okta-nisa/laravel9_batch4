<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Peran;
use Illuminate\Http\Request;

class PeranController extends Controller
{
    //
    public function tambah(Request $request, $id){
        
        $request->validate([
        "nama" => 'required'
        ]);

        $peran = new Peran;

        $peran->nama = $request->nama;
        $peran->film_id = $id;
        $peran->cast_id = 1;
        
        $peran->save();

        return redirect('/film/', $id);
        

    }
    

}
