<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function halamanUtama(){
        return view('home');
    }

    public function halamanDataTable(){
        return view('data-table');
    }
    
}
