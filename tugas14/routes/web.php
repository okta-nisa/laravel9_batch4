<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PeranController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {
    
    Route::get('/cast/create', [CastController::class, 'create'])->name('createCast');
    Route::post('/cast',[CastController::class,'store'])->name('storeCast');
    
    Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('castEdit');
    Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('updateCast');
    Route::delete('/cast/{cast_id}',[CastController::class, 'destroy'])->name('destroyCast');

    Route::get('/', [IndexController::class, 'halamanUtama'])->name('halamanUtama');
    Route::get('/register', [AuthController::class, 'halamanRegister'])->name('halamanRegister');
    Route::post('/welcome', [AuthController::class, 'halamanWelcome'])->name('halamanWelcome');
    Route::get('/data-tables', [IndexController::class, 'halamanDataTable'])->name('halamanDataTable');

    Route::resource('profile', ProfileController::class)->only(['index', 'update']);

    Route::get('/genre/create', [GenreController::class, 'create'])->name('createGenre');
    Route::post('/genre',[GenreController::class,'store'])->name('storeGenre');

    Route::get('/film/create', [FilmController::class, 'create'])->name('createFilm');
    Route::post('/film',[FilmController::class,'store'])->name('storeFilm');

    Route::post('/peran/{film_id}',[PeranController::class,'tambah'])->name('tambahPeran');


});

Route::get('/cast', [CastController::class, 'index'])->name('indexCast');
Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('showCast');

Route::get('/genre', [GenreController::class, 'index'])->name('indexGenre');
Route::get('/genre/{genre_id}', [GenreController::class, 'show'])->name('showCast');

Route::get('/film', [FilmController::class, 'index'])->name('indexFilm');
Route::get('/film/{film_id}', [FilmController::class, 'show'])->name('showFilm');






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

